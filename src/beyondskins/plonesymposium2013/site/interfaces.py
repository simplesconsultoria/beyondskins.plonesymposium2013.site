# -*- coding: utf-8 -*-
'''
Created on 07/04/2013

@author: jpg
'''
from plone.theme.interfaces import IDefaultPloneLayer

class IThemeSpecific(IDefaultPloneLayer):
    """Marker interface that defines a Zope 3 skin layer bound to a Skin
       Selection in portal_skins.
    """
